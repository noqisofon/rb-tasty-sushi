# -*- coding: utf-8; -*-
require_relative 'spec_helper'

require_relative  '../lib/sushi'


describe Sushi do

  def tasty?(food)
    food.tasty?
  end

  it '寿司はおいしい' do
    a_sushi =    described_class.new

    expect( tasty? a_sushi ).to be_truthy
  end

end
